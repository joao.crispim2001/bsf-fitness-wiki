# Workout nutrition

It is important to consider workout nutrition if you would like to optimise performance, recovery and muscle building. 

# Pre workout nutrition

* Ideally pre workout you should consume foods high in fast disgesting carbohydrates (foods high in carbs, but low in fibre)

* Eat foods low in fat, fats slow down digestion which is not what you want before a workout as fast digesting carbohydrates are going to be the main source of energy in resistance training. 

* Hydrate

# Intraworkout nutrition

* Optimally it would be good to get in a highly branched cyclic dextrin during a workout, along with EAAs. Both digest extremely fast which is great for when you need fuel during a long hard session. 

* Cyclic dextrin is a low molecular weight carbohydrate that causes little processing by your gastrointestinal track, only down side to this is the expense of the supplement. 

* Overall intraworkout nutrition has a small benefit and is probably something you should consider doing if you're looking to be as efficient as you can (i.e looking to compete in bodybuilding). For the average gym goer it is not very important

# Postworkout nutrition

* Post workout nutrition is extremely important as it has an effect on your recovery. If you can recovery faster you can deal with more overall volume, which can lead to more muscle growth.

* Good postworkout nutrition would be similar to pre workout nutrition (fast digesting carbs, low fibre, and low fat). The reason for this is because glut-4 receptors are upregulated post workout therefore your ability to absorb carbohydrates is a lot higher so its a good opportunity to intake faster digesting carbs. 

* Post workout it is a good idea to relax, and get into a parasympathetic state. Protein consumption post workout is thought to be important, which it is but that doesn't mean you have to chug down a protein shake right after.

* Take your time with ingestion of protein after a workout, it isn't essential. 

* Hydrate