# WIP

# At home training program

It is possible to build a great physique without spending too much money on a gym membership or expensive equipment.
But if you are planning to train from home you should consider buying the following equipment to make training easier and more fun.

* Pull up bar
* A dumbell set, preferably over 2x12kg

If you are planning to spend a little more money on a home gym the following equipment is recommended:

* Barbell
* Adjustable Bench
* Squat rack

# Decent at home training program

|            | M | T | W | T | F | S | S |
|---         |---|---|---|---|---|---|---|
|Bodybuilding|X  |   |X  |   |X  |   |   |

### Monday Push -> Pull --> legs

| Lift | Weight | Reps | Sets |
|------|--------|------|------|
|Weighted Push Up |
|DB Shoulder press|
|Chin up|
|DB Row|
|DB Lunge|
|SDL|

### Tuesday, Pull--> Legs--> Push

| Lift | Weight | Reps | Sets |
|------|--------|------|------|
|DB Lunge|
|SDL|
|Weighted Push Up |
|DB Shoulder press|
|Chin up|
|DB Row|

### Friday, Legs --> Push --> Pull

| Lift | Weight | Reps | Sets |
|------|--------|------|------|
|Chin up|
|DB Row|
|DB Lunge|
|SDL|
|Weighted Push Up |
|DB Shoulder press|

# I don't want to buy equipment training program

|            | M | T | W | T | F | S | S |
|---         |---|---|---|---|---|---|---|
|Bodybuilding|X  |   |X  |   |X  |   |   |

### Monday

| Lift | Weight | Reps | Sets |
|------|--------|------|------|
|Weighted Push Up |
|split squat or beginner shrimps|
|Pull up|



