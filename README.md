![alt text](BSF-logo.png)

# Beginner guide
- [x] [Beginner basics](beginner/beginner-basics.md)
- [x] [Get a small home gym started](resistance-training/buying_equipment.md)

# Mindset & Motivation

- [ ] [Lifting won't fix your life](https://youtu.be/bhoWS1l1tFk?t=588)
- [ ] [Motivation, disipline and dedication](mindset-and-motivation/Motivation-disipline-dedication.md)
- [ ] [Body dismorphia and preventing eating disorders](mindset-and-motivation/disorders.md)
- [ ] [I skipped a workout and now I feel bad](mindset-and-motivation/skipping-workouts.md)
- [ ] [Should I get a training buddy? Independence and consistancy](blabla)

<!------------------------------------------------------------------>

# Sleep

- [ ] [Sleep basics](sleep/sleep.md)
- [ ] [Snorring and sleep apnea](https://www.heart.org/en/health-topics/sleep-disorders/sleep-apnea-and-heart-disease-stroke)

<!------------------------------------------------------------------>

# Cardio

* [ ] [Cardio basics](cardio/cardio-basics.md)

# Resistance Training

* [ ] [Excersice library](resistance-training/excersice-library.md)
* [ ] [hypertrophy Basics](resistance-training/hypertrophy-basics.md)
* [ ] [Teen bodybuilding](resistance-training/teen-bodybuilding.md)
* [ ] Training programs:
    * [ ] [What routine should I do as a beginner](resistance-training/beginner-routine.md)
    * [ ] [Resistance training at home](resistance-training/at-home-resistance-training.md)
    * [ ] [Staff training programs](training-programs/Leigh's_workout.md)
### Muscle measurements and comparison tools

* https://bodywhat.com/
* https://ffmicalculator.org/
* https://www.calculator.net/bmi-calculator
* https://symmetricstrength.com/

<!------------------------------------------------------------------>

# Nutrition

* [ ] [Creatine](nutrition/creatine_supplementation.md)
* [ ] [What food should I eat?](nutrition/what-to-eat.md)
* [ ] [Fat loss basics](nutrition/fat-loss-basics.md)
* [ ] [Bodybuilding diet](nutirition/bodybuilding-diet.md)
* [ ] [Bodybuilding nutrition](workout_nutrition.md)
* [ ] Hydration + salt

<!------------------------------------------------------------------>

# Recommended external blogs/channels

Bodybuilding/fitness:
* [Jeff Nippard](https://www.youtube.com/user/icecream4PRs)
* [IFBB PRO JAMES HOLLINGSHEAD](https://www.youtube.com/channel/UCD8wepfBT-yDt4pmI8-z5bg)
* [Thomas Maw](https://www.youtube.com/c/TMCycles)
* [A.J Morris](https://www.youtube.com/channel/UCz6Mly4rhmeML0zDjo6yDrg)
* [Cameron Mackay](https://www.youtube.com/channel/UCo3dKndpV6HMUU1z5IQ-fTw)
* [Finn Kelly](https://www.youtube.com/channel/UCr8hEuU76Ww5JvlFzpW4dDQ)
* [Josh Bridgman](https://www.youtube.com/channel/UCg_1gRpsCuq-s-w54UqANfA)
* [KHIFIE WEST](https://www.youtube.com/channel/UCA4KRYGKV9Me7qxRaVXfVww)
* [Kuba Cielen](https://www.youtube.com/channel/UCIfNocTBJECCId_oW11YeRw)
* [Scooby werkstat](https://www.youtube.com/user/VitruvianPhysique)
* [Mark Coles M10](https://www.youtube.com/channel/UCZ7yH-kLdFj7HNo2Dcs6FnQ)
* [UK PRO MUSCLE PODCAST](https://www.youtube.com/channel/UC7cZErjKpG3xX7QrSkpC6aw)

strength training:
* [Alen Thrall](https://www.youtube.com/channel/UCRLOLGZl3-QTaJfLmAKgoAw)

Cooking and nutrition:
* [Ethan Chlebowski](https://www.youtube.com/channel/UCDq5v10l4wkV5-ZBIJJFbzQ)