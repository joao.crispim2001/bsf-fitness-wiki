# Leigh's workout 
 
Information-
* Top set means a heavy set usually 5-8 reps. 

* Back off set means a lighter set take (10-30% off) typically 10-20 reps. 

* Longgg rest periods (this is a home workout for me so I can rest as long as I want typically 5 mins)

* Progressively overloaded, top sets and back off sets are always progressed by adding weight or adding more reps to previous session.

* RPE (how much effort the set was out of 10) RPE past >10 means forced reps, partials and intensity techniques. 


# Day 1, legs with OHP

|LIFT|SETS|REPS|RPE|
|---|---|---|---|
|Squat|3-4|1 top set, 2-3 back off|9-10|
|BB or db lunges|2|12-20|9-10|
|RDL|3|1 top set, 2 back off|8-9|
|Seated OHP|2|1 top set, 1 back off|9-10|

# Day 2, Push 

|LIFT|SETS|REPS|RPE|
|---|---|---|---|
|Bench press|3|1 top set, 2 back off|8-9|
|Incline bench/incline dumbbell|3|1 top set, 2 back off|9 barbell, 10 with dumbbell|
|Push ups|2|As many as possible|10|
|Dumbbell lateral raises|3|12-20|Set 1 and 2 (7-8) last set 12|
|Close grip bench or skull crushers|3|1 top set, and 2 back off|8-9|

# Day 3, Pull 

|LIFT|SETS|REPS|RPE|
|---|---|---|---|
|Barbell row|2|1 top set, 1 back off|10|
|wide grip pull up|3|As many as possible|10|
|Meadow row or dumbbell row|2-3|12-15|9-10|
|Barbell bicep curl|3|1 top set, 2 back off|9-10|
|Dumbbell hammer curl|2|12-15|10|

# Day 4, rest and repeat