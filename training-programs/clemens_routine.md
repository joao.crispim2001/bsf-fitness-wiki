# Clemens' new workout, WIP

TODO's:
* Shoulders or triceps to leg day?
* Ajust volume leg day?
* Replace skullcrushers when pl8's arive

|M  |T  |W  |T  |F  |S  |S  |M  |...|
|---|---|---|---|---|---|---|---|---|
|A  |B  |C  |   |A  |B  |C  |   |...|

# A, PUSH
|LIFT|SETS|REPS|
|---|---|---|
|Floor chest press|3|6-12|
|DB chest fly, floor|3|6-12|
|BB OPH|3|6-12|
|DB OPH|3|6-12|
|SUPERSET:|3|
| - Skullcrushers||6-12|
| - Diamond push ups||6-12|

# B, PULL
|LIFT|SETS|REPS|
|---|---|---|
|Pull up|3|6-12|
|BB Pedlay row|3|6-12|
|Chin up|3|6-12|
|DB Curl|3|6-12|
|DB Curl, hammer|3|6-12|

# C, LEGS

|LIFT|SETS|REPS|
|---|---|---|
|Pl8 Lunges|3-4|12-24|
|BB SLDL|3-4|6-12|
|PL8 belt, single leg, calve raises|3|6-12|
