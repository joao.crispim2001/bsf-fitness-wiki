# What food should I eat?

Your caloric budget should be spend to get the following micronutients.

# Protein

High quallity Protein Sources:
* 90% Lean ground beef
* Chicken breast
* Other lean cuts of red and white meat.
* Whey Isolate
* Eggs or egg whites
* white and fat Fish
* Soft cheeses like cottage cheese, greek yoghurt
* Fat free milk (or lactose free for and even better protein ratio)

# Fats

* High omega 3 fish (salmon, sardines, mackerel, cod liver).
* Nuts (Brazil nuts, almonds, cashews, walnuts)- omega 6 primarily
* Nut butters (Peanut butter, almond butter)- omega 6 primarily 
* Seeds (flaxseed, chaiseed) omega 3 and omega 6
* Oils such (Olive oil and coconut oil)
* Grassfed butter 

# Carbs

* Granola
* White/brown rice
* White/brown pasta
* Wholegrain bread
* Berries, high in antioxidants and vitamins/minerals
* Veggies, high in antioxidants and vitamins/minerals
* Fruits 


# Fiber

TODO

# Vitamins and minerals

* Eat lots of fruit/vegetables
* Fish 
* Grassfed dairy products
* Eggs
* Red meat
* Seafood
* Vitamin supplementation, especially vitamin D and Omega 3


# Videos on low calorie and high calorie dense foods

* https://www.youtube.com/watch?v=iXD3c3qDE5s
* [Ethan Chlebowski lower calorie series](https://www.youtube.com/results?search_query=+Ethan+Chlebowski+lower+calorie+series) Professional chef creates accessible meals. They taste amazing and are diverse.

# This is all too difficult, create a meal plan for meal

* https://www.custommealplanner.com/ Good accurate meal planner, food is cheap and tastes bland but decent.